﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace calculate
{
    public class PolandNotation
    {
        private string StringToCalculate { get; set; }
        private Stack<char> OperationStack;
        private Stack<double> ValuesStack; 
        private bool UnarityAllowed { get; set; }
        public PolandNotation(string stringToCalculate)
        {
            this.StringToCalculate = stringToCalculate.Replace(" ", "");
            this.ValuesStack = new Stack<double>();
            this.OperationStack = new Stack<char>();
            this.UnarityAllowed = true;
        }
        public bool IsOperator(char c)
        {
            return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
        }
        public bool IsLetter(char c)
        {
            return (c >= 'a' && c <= 'z');
        }
        public bool IsNumeral(char c)
        {
            return (c >= '0' && c <= '9');
        }
        public int Priority(char oper)
        {
            var priority = 0;
            if (oper == '?' || oper == '&')
            {
                priority = 4;
            }
            else {
                if (oper == '+' || oper == '-')
                {
                    priority = 1;
                }
                else {
                    if (oper == '*' || oper == '/')
                    {
                        priority = 2;
                    }
                    else {
                        if (oper == '^')
                        {
                            priority = 3;
                        }
                        else
                        {
                            priority = -1;
                        }
                    }
                }
            }
            return priority;
        }
        public void CountOperation(Stack<double> st, char oper)
        {
            if (oper == '?' || oper == '&')
            {
                double left = this.ValuesStack.Peek();
                this.ValuesStack.Pop();
                switch (oper)
                {
                    case '&': this.ValuesStack.Push(left);
                        break;
                    case '?': this.ValuesStack.Push(-left);
                        break;
                }
            }
            else {
                double right = this.ValuesStack.Peek();
                this.ValuesStack.Pop();
                double left = this.ValuesStack.Peek();
                this.ValuesStack.Pop();
                switch (oper)
                {
                    case '+':
                        this.ValuesStack.Push(left + right);
                        break;
                    case '-':
                        this.ValuesStack.Push(left - right);
                        break;
                    case '*':
                        this.ValuesStack.Push(left * right);
                        break;
                    case '/':
                        this.ValuesStack.Push(left / right);
                        break;
                    case '^':
                        this.ValuesStack.Push(Math.Pow(left, right));
                        break;
                }
            }
            this.OperationStack.Pop();
        }
        public double Calculate()
        {
            for (var i = 0; i < this.StringToCalculate.Length; i++)
            {
                var CurrentSymbol = this.StringToCalculate[i];
                if (this.IsLetter(CurrentSymbol))
                {
                    Console.Write("Введите значение '" + CurrentSymbol.ToString() + "':   ");
                    var Value = Console.ReadLine().Replace(".", ",");
                    var IntValue = double.Parse(Value);
                    var Str = "";
                    if (i > 0 && this.IsNumeral(this.StringToCalculate[i - 1]))
                    {
                        if (this.StringToCalculate[i - 1] != '*')
                        {
                            Str = "*(" + IntValue.ToString().Replace(",", ".") + ")";
                        }
                        else
                        {
                            Str = "(" + IntValue.ToString().Replace(",", ".") + ")";
                        }
                    }
                    else
                    {
                        Str = "(" + IntValue.ToString().Replace(",", ".") + ")";
                    }
                    this.StringToCalculate = this.StringToCalculate.Replace(CurrentSymbol.ToString(), Str);
               }
            }
            for (var i = 0; i < this.StringToCalculate.Length; i++)
            {
                char CurrentSymbol = this.StringToCalculate[i];
                if (CurrentSymbol == '(')
                {
                    this.UnarityAllowed = true;
                    this.OperationStack.Push(CurrentSymbol);
                }
                else
                {
                    if (CurrentSymbol == ')')
                    {
                        while (this.OperationStack.Peek() != '(')
                        {
                            CountOperation(this.ValuesStack, this.OperationStack.Peek());
                        }
                        this.OperationStack.Pop();
                        this.UnarityAllowed = false;
                    }
                    else
                    {
                        if (this.IsOperator(CurrentSymbol))
                        {
                            if (this.UnarityAllowed == true)
                            {
                                if (CurrentSymbol == '-')
                                {
                                    CurrentSymbol = '?';
                                }
                                if (CurrentSymbol == '+')
                                {
                                    CurrentSymbol = '&';
                                }
                                this.UnarityAllowed = false;
                            }
                            while (this.OperationStack.Count() != 0 && this.Priority(this.OperationStack.Peek()) >= this.Priority(CurrentSymbol))
                            {
                                this.CountOperation(this.ValuesStack, this.OperationStack.Peek());
                            }
                            this.OperationStack.Push(CurrentSymbol);
                        }
                        else
                        {
                            string Operand = "";
                            while (i < this.StringToCalculate.Count() && (this.IsNumeral(this.StringToCalculate[i]) || this.StringToCalculate[i] == '.'))
                            {
                                Operand += this.StringToCalculate[i++];
                            }
                            i--;
                            Operand = Operand.ToString().Replace(".", ",");
                            var Op = double.Parse(Operand);
                            this.ValuesStack.Push(double.Parse(Operand));
                            this.UnarityAllowed = false;

                        }
                    }
                }
            }
            while(true)
            {
                if (this.OperationStack.Count() > 0)
                {
                    this.CountOperation(this.ValuesStack, this.OperationStack.Peek());
                    if (this.ValuesStack.Count() <= 1)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            return this.ValuesStack.Peek();
        }
    }
  
    class Program
    {
        static void Main(string[] args)
        {
            string expression = Console.ReadLine();
            PolandNotation P = new PolandNotation(expression);
            Console.Write(P.Calculate().ToString().Replace(",", "."));
        }
    }
}
