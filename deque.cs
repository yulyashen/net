﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;

class EmptyDequeException : ApplicationException
{
    public EmptyDequeException(): base("Deque is empty")
    { }
}
public class Node<T>
{
    private T Value { get; set; }
    private Node<T> Next { get; set; }
    private Node<T> Prev { get; set; }
    public Node(T value)
    {
        this.Next = null;
        this.Prev = null;
        this.Value = value;
    }
    public Node<T> GetNext()
    {
        return this.Next;
    }
    public Node<T> GetPrev()
    {
        return this.Prev;
    }
    public void SetNext(Node<T> next)
    {
        this.Next = next;
    }
    public void SetPrev(Node<T> prev)
    {
        this.Prev = prev;
    }
    public T GetValue()
    {
        return this.Value;
    }
    public void SetValue(T value)
    {
        this.Value = value;
    }
}
public class DequeEnumerator<T> : IEnumerator<Node<T>>
{
    public int Position { get; set; }
    public Node<T> Current { get; set; }
    public int DequeSize { get; }
    public Node<T> BeginNode { get; set; }
    public Node<T> EndNode { get; set; }

    public DequeEnumerator(Deque<T> deque)
    {
        this.Position = -1;
        this.Current = null;
        this.DequeSize = deque.GetSize();
        this.BeginNode = deque.GetHead();
        this.EndNode = deque.GetTail();
    }
    void IDisposable.Dispose() { }
    object IEnumerator.Current
    {
        get { return GetCurrentNode(); }
    }
    Node<T> IEnumerator<Node<T>>.Current
    {
        get { return GetCurrentNode(); }
    }
    public Node<T> GetCurrentNode()
    {
        return this.Current;
    }
    public bool MoveNext()
    {
        if ((this.Position < this.DequeSize - 1) && (this.Position >= 0))
        {
            this.Position++;
            this.Current = this.Current.GetNext();
            return true;
        }
        else
        {
            if ((this.Position == -1) && (this.DequeSize > 0))
            {
                this.Position++;
                this.Current = this.BeginNode;
                return true;
            }
            else {
                return false;
            }
        }
    }
    public bool MovePrev()
    {
        if (this.Position != 0)
        {
            this.Position--;
            this.Current = this.Current.GetPrev();
            return true;
        }
        else
        {
            return false;
        }
    }
    public void Reset()
    {
        this.Current = null;
        this.Position = -1;
    }
    public DequeEnumerator<T> Begin()
    {
        this.Position = 0;
        this.Current = this.BeginNode;
        return this;
    }
}
public class Deque<T> : IEnumerable
{
    private int Size { get; set; }
    private Node<T> Head { get; set; }
    private Node<T> Tail { get; set; }
    public Deque()
    {
        this.Head = null;
        this.Tail = null;
        this.Size = 0;
    }
    public void PushBack(T element)
    {
        Node<T> newNode = new Node<T>(element);
        if (this.Size == 0)
        {
            this.Head = newNode;
        }
        else
        {
            newNode.SetPrev(this.Tail);
            this.Tail.SetNext(newNode);
        }
        this.Tail = newNode;
        this.Size++;
    }
    public void PushFront(T element)
    {
        Node<T> newNode = new Node<T>(element);
        if (this.Size == 0)
        {
            this.Tail = newNode;
        }
        else
        {
            newNode.SetNext(this.Head);
            this.Head.SetPrev(newNode);
        }
        this.Head = newNode;
        this.Size++;
    }
    public T PopBack()
    {
        if (this.Size == 0)
        {
            throw new EmptyDequeException();
        }
        Node<T> deletedNode = this.Tail;
        if (this.Tail.GetPrev() != null)
        {
            this.Tail.GetPrev().SetNext(null);
        }
        this.Tail = this.Tail.GetPrev();
        this.Size--;
        return deletedNode.GetValue();
    }
    public T PopFront()
    {
        if (this.Size == 0)
        {
            throw new EmptyDequeException();
        }
        Node<T> deletedNode = this.Head;
        if (this.Head.GetNext() != null)
        {
            this.Head.GetNext().SetPrev(null);
        }
        this.Head = this.Head.GetNext();
        this.Size--;
        return deletedNode.GetValue();
    }
    public T Front()
    {
        if (this.Size == 0)
        {
            throw new EmptyDequeException();
        }
        return this.Head.GetValue();
    }
    public T Back()
    {
        if (this.Size == 0)
        {
            throw new EmptyDequeException();
        }
        return this.Tail.GetValue();
    }
    public Node<T> GetHead()
    {
        if (this.Size == 0)
        {
            throw new EmptyDequeException();
        }
        return this.Head;
    }
    public Node<T> GetTail()
    {
        if (this.Size == 0)
        {
            throw new EmptyDequeException();
        }
        return this.Tail;
    }
    public int GetSize()
    {
        return this.Size;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return (IEnumerator)GetEnumerator();
    }
    public DequeEnumerator<T> GetEnumerator()
    {
        return new DequeEnumerator<T>(this);
    }
    public void ForEach(Action<Node<T>> action)
    {
        foreach(var i in this)
        {
            action(i);
        }
    }

}


namespace Dequeue
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Action<Node<int>> action;
                action = s => {if (s.GetValue() % 2 == 0) { Console.Write(s);}}
                Action<Node<int> > message;
                message = s => Console.Write("***" + s.GetValue().ToString() + "***\n");
                Deque<int> d = new Deque<int>();
                d.PushBack(3);
                d.PushFront(4);
                d.PushBack(5);
                d.PushFront(6);
                foreach (var i in d)
                {
                    if (i.GetValue() % 2 == 0){
                        message(i);
                    }
                }
                d.ForEach(action);
                IEnumerable<int> it =
                    from num in d
                    where num % 2 == 0
                    select num;
            }
            catch (EmptyDequeException e)
            {
                Console.Write(e.GetBaseException());
            }
            catch (Exception)
            {
                Console.Write("Exception");
            }
        }
    }
}
